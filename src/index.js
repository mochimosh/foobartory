import Vue from 'vue';
import Vuex from 'vuex';
import App from './app';
import warehouse from './warehouse';

Vue.use(Vuex);

new Vue({
    el: '#app',
    store: new Vuex.Store(warehouse),
    template: "<App/>",
    components: { App },
});