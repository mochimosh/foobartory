import Vuex from 'vuex'
import robot from './robot';

export default {
    state: {
        robots: [robot, robot],
        foo: [],
        bar: [],
        foobar: [],
        fail: [],
        sold: [],
        money: 0,
    },
    mutations: {
        addFoo(state, id) {
            state.foo.push(id);
        },
        addBar(state, id) {
            state.bar.push(id);
        },
        addFooBar(state) {
            state.foobar.push(`${state.foo[0]}-${state.bar[0]}`);
            state.foo.splice(0, 1);
            state.bar.splice(0, 1);
        },
        failFooBar(state) {
            state.fail.push(`${state.foo[0]}`);
            state.foo.splice(0, 1);
        },
        addMoney(state, toSell) {
            state.money = state.money + toSell;
            state.foobar.splice(0, toSell);
        },
        createRobot(state) {
            state.money = state.money - 3;
            state.foo.splice(0, 2);
            state.robots.push(robot);
        }
    }
};