# FooBarTory !
Pour réaliser le test j'ai utilisé html et javascript (ES6). 
Je me suis servi du framework vuejs et ses composants (vuex, vue-compiler), ainsi que de webpack pour compiler tout ça. 

**Pourquoi vuejs ?** 
J'ai utilisé ce framework pour sa simplicité et comme vous cherchez plus un dev front, cela me semblait être parfait pour réaliser le test.

# Prérequis
- npm
- serveur apache

J'ai laissé exprès tous les fichiers afin que vous puissiez y jeter un coup d'oeil. Si besoin je peux vous envoyer les fichiers compilés.

# Mise en place
Dans le dossier: 

    npm install
    npm run build

Si vous voulez lancer le mode dev

    npm start
